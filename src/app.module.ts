import { Module } from '@nestjs/common';

@Module({
  providers: [
    // { provide: 'foo', useValue: undefined  },
    { provide: 'foo', useFactory: () => undefined  },
  ],
  exports: ['foo']
})
export class AppModule {}
